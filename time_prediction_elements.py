################################################################################
# Copyright (c) 2019, Lawrence Livermore National Security, LLC.
# Produced at the Lawrence Livermore National Laboratory (LLNL).
# Prepared by LLNL under Contract DE-AC52-07NA27344.
# Written by Thomas Desautels, desautels2@llnl.gov
#
# NOT CLEARED FOR OUTSIDE RELEASE
# All rights reserved.
#
# THIS SOFTWARE IS PRESENTLY FOR LLNL USE ONLY. NO OUTSIDE USE OR RELEASE IS
# PERMITTED. The above copyright notice and this permission notice must be
# included with any copies or substantial portions of the software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
################################################################################

"""

Evaluate the speed & numerics of prediction with a GPT model

"""

import pandas as pd
import numpy as np
import torch
import gpytorch
from gpytorch.constraints import Interval
import pickle
import copy
from datetime import datetime, timedelta
import warnings

def get_layer_sizes(state_dict):
    """
    Using a torch state_dict, obtain the lin_layers sizes

    :param state_dict: PyTorch state_dict
    :return: dictionary of lists of layer sizes (ints)
    """

    # TODO: Update for the case where we have two separate streams of linear layers

    re_matcher = '(lin_layers[_a-zA-Z]*)\.([0-9]+)\.bias'
    layers = {}
    for ki, vi in state_dict.items():
        v = re.fullmatch(re_matcher, ki)
        if not v:
            continue
        if not v.group(1) in layers.keys():
            layers[v.group(1)] = {}
        layers[v.group(1)][int(v.group(2))] = vi.shape

    layer_sizes = {
        ki: [vi[j][0] for j in sorted(list(vi.keys()))]
        for ki, vi in layers.items()
    }
    return layer_sizes

def get_num_tasks(state_dict):
    """
    Using a torch state_dict, obtain the size of the task covariance kernel

    :param state_dict:
    :return: int, giving the number of tasks
    """
    return int(state_dict['task_covar_module.covar_factor'].shape[0])

def print_parameters(model, likelihood):
    # Print out the learned model parameters:
    print('\n--------')
    print('Learned noise parameters:')
    for ni, parami in likelihood.named_parameters():
        print('{}: {}'.format(ni, parami))
    try:
        lnc = likelihood.noise
        print('Noise: {}'.format(lnc))
    except:
        pass
    print('------')
    print('Learned model parameters:')
    for ni, parami in model.named_parameters():
        print('{}: {}'.format(ni, parami))

    try:
        tcm = model.task_covar_module.covar_matrix.evaluate()
        print('Task covariance matrix:')
        print(tcm)
    except:
        pass

    try:
        lengthscales = model.covar_module.lengthscale
        print('Lengthscales in the kernel:')
        print(lengthscales)
    except:
        pass
    print('------')

class MultitaskVanillaGPModelDKL(gpytorch.models.ExactGP):
    def __init__(self, train_x, train_y, likelihood, layer_sizes=None, deltas=False, num_tasks=2):
        super(MultitaskVanillaGPModelDKL, self).__init__(train_x, train_y, likelihood)

        if layer_sizes is None:
            layer_sizes = []

        # Set up the NN layers preceding the GP
        self.lin_layers = []
        for i, size_i in enumerate(layer_sizes):
            if i == 0:
                input_size = train_x[0].shape[1]
            else:
                input_size = layer_sizes[i-1]
            self.lin_layers.append(torch.nn.Linear(input_size, size_i))
        self.lin_layers = torch.nn.ModuleList(self.lin_layers)
        for i, lin_layer in enumerate(self.lin_layers):
            torch.nn.init.eye_(lin_layer.weight.data)
            if i == 0 and deltas:
                print('Initializing with delta structure!')
                # Assume that the predictor columns have been ordered such that
                # the input is mutant_(feature_0), ..., mutant(feature_k),
                # wt_(feature_0), ... wt(feature_k), i.e., that the
                input_size = lin_layer.weight.shape[1]
                assert input_size == 2 * math.floor(input_size/2.0)  # I.e., it's even
                for colpos in range(math.floor(input_size/2.0)):
                    colneg = colpos + math.floor(input_size/2.0)
                    # print(colpos, colneg)
                    lin_layer.weight.data[:, colneg] = \
                        -1.0 * lin_layer.weight.data[:, colpos]
            lin_layer.weight.data = 0.1 * lin_layer.weight.data + 0.005 * torch.randn_like(lin_layer.weight.data)

        if layer_sizes == []:
            layer_sizes = [train_x[0].shape[1]]

        self.num_tasks = num_tasks

        # Set up the GP on top
        self.mean_module = gpytorch.means.ConstantMean()

        if self.num_tasks > 1:
            self.covar_module = gpytorch.kernels.RBFKernel()

            # We learn an IndexKernel for num_tasks tasks
            self.task_covar_module = gpytorch.kernels.IndexKernel(
                num_tasks=num_tasks, rank=num_tasks-1)
        else:
            self.covar_module = gpytorch.kernels.ScaleKernel(gpytorch.kernels.RBFKernel())
            self.task_covar_module = gpytorch.kernels.RBFKernel()  # Shouldn't do anything

    def forward(self, x, i):
        for idxi, lli in enumerate(self.lin_layers):
            if idxi == len(self.lin_layers):
                x = lli(x)
            else:
                x = torch.tanh(lli(x))

        mean_x = self.mean_module(x)

        # Get input-input covariance
        covar_x = self.covar_module(x)
        if self.num_tasks > 1:
            # Get task-task covariance
            covar_i = self.task_covar_module(i)
            # Multiply the two together to get the covariance we want
            covar = covar_x.mul(covar_i)  # TODO: Switch to ProductStructureKernel?
        else:
            covar = covar_x

        return gpytorch.distributions.MultivariateNormal(mean_x, covar)

    def get_fantasy_model(self, inputs, targets, **kwargs):
        try:
            model_out = super(MultitaskVanillaGPModelDKL, self).get_fantasy_model(inputs, targets, **kwargs)
        except RuntimeError:
            warnings.warn('Guarding failed get_fantasy_model call', RuntimeWarning)
            model_out = deepcopy(self)
            # TODO: Don't I want a model_out.set_train_data here?

        return model_out

class MultitaskLinPlusStationaryDKLGP(gpytorch.models.ExactGP):
    def __init__(self, train_x, train_y, likelihood, layer_sizes_lin=None, layer_sizes_stationary=None, deltas=False, num_tasks=2):
        super(MultitaskLinPlusStationaryDKLGP, self).__init__(train_x, train_y, likelihood)

        # Input handling:
        if layer_sizes_lin is None:
            layer_sizes_lin = []

        if layer_sizes_stationary is None:
            layer_sizes_stationary = []

        # Set up the linear transformations going into the linear kernel
        self.lin_layers_lin = []
        for i, size_i in enumerate(layer_sizes_lin):
            if i == 0:
                input_size = train_x[0].shape[1]
            else:
                input_size = layer_sizes_lin[i-1]
            self.lin_layers_lin.append(torch.nn.Linear(input_size, size_i))
        self.lin_layers_lin = torch.nn.ModuleList(self.lin_layers_lin)

        # Set up the linear transformations going into the stationary kernel
        self.lin_layers_stationary = []
        for i, size_i in enumerate(layer_sizes_stationary):
            if i == 0:
                input_size = train_x[0].shape[1]
            else:
                input_size = layer_sizes_stationary[i - 1]
            self.lin_layers_stationary.append(torch.nn.Linear(input_size, size_i))
        self.lin_layers_stationary = torch.nn.ModuleList(self.lin_layers_stationary)

        # Initialize all of the weights in the linear layers
        for lin_layer_group in [self.lin_layers_lin, self.lin_layers_stationary]:
            for i, lin_layer in enumerate(lin_layer_group):
                torch.nn.init.eye_(lin_layer.weight.data)
                if i == 0 and deltas:
                    print('Initializing with delta structure!')
                    # Assume that the predictor columns have been ordered such that
                    # the input is mutant_(feature_0), ..., mutant(feature_k),
                    # wt_(feature_0), ... wt(feature_k), i.e., that the
                    input_size = lin_layer.weight.shape[1]
                    assert input_size == 2 * math.floor(
                        input_size / 2.0)  # I.e., it's even
                    lin_layer.weight.data[:, :math.floor(input_size / 2.0)] = \
                        lin_layer.weight.data[:, torch.randperm(
                            math.floor(input_size / 2.0)
                        )]
                    lin_layer.weight.data[:, math.floor(input_size / 2.0):] = \
                        -1.0 * lin_layer.weight.data[:, :math.floor(input_size / 2.0)]
                lin_layer.weight.data = 0.05 * lin_layer.weight.data + 0.005 * torch.randn_like(
                    lin_layer.weight.data)
                lin_layer.zero_grad()

        # Set the number of tasks
        self.num_tasks = num_tasks

        # Set up the (dual kernel) GP on top
        self.mean_module = gpytorch.means.ConstantMean()

        # Initialize the dimension lists for input to the kernels
        if not layer_sizes_stationary:
            layer_sizes_stationary = [train_x[0].shape[1]]
        stationary_dims = [i for i in range(layer_sizes_stationary[-1])]

        if not layer_sizes_lin:
            layer_sizes_lin = [train_x[0].shape[1]]
        linear_dims = [i + stationary_dims[-1] + 1 for i in range(layer_sizes_lin[-1])]

        if self.num_tasks > 1:
            self.covar_module = \
                gpytorch.kernels.RBFKernel(
                    active_dims=torch.tensor(stationary_dims)
                ) +\
                gpytorch.kernels.LinearKernel(
                    active_dims=torch.tensor(linear_dims)
                )

            # We learn an IndexKernel for num_tasks tasks
            self.task_covar_module = gpytorch.kernels.IndexKernel(
                num_tasks=num_tasks, rank=num_tasks-1)

        else:
            self.covar_module = gpytorch.kernels.ScaleKernel(
                gpytorch.kernels.RBFKernel(
                    active_dims=torch.tensor(stationary_dims)
                ) + \
                gpytorch.kernels.LinearKernel(
                    active_dims=torch.tensor(linear_dims)
                )
            )
            self.task_covar_module = gpytorch.kernels.RBFKernel()  # Shouldn't do anything

    def initialize_task_covar(self):
        est_covar_matrix = estimate_task_covariance_from_data(
            self.train_inputs[0], self.train_targets, self.num_tasks)
        truncated_eigvecs, diag_residual = low_rank_and_diag_decomp(
            est_covar_matrix, rank=self.num_tasks - 1
        )
        self.task_covar_module.raw_var = torch.nn.Parameter(
            torch.log(torch.exp(diag_residual) - 1.0)
        )
        self.task_covar_module.covar_factor = torch.nn.Parameter(
            truncated_eigvecs
        )

    def forward(self, x, i):
        xlin = x.clone()
        for idxi, lli in enumerate(self.lin_layers_lin):
            if idxi == len(self.lin_layers_lin):
                xlin = lli(xlin)
            else:
                xlin = torch.tanh(lli(xlin))

        xstationary = x.clone()
        for idxi, lli in enumerate(self.lin_layers_stationary):
            if idxi == len(self.lin_layers_stationary):
                xstationary = lli(xstationary)
            else:
                xstationary = torch.tanh(lli(xstationary))

        xall = torch.cat([xstationary, xlin], dim=1)

        mean_x = self.mean_module(xall)

        # Get input-input covariance
        covar_x = self.covar_module(xall)
        if self.num_tasks > 1:
            # Get task-task covariance
            covar_i = self.task_covar_module(i)
            # Multiply the two together to get the covariance we want
            covar = covar_x.mul(covar_i)  # TODO: Switch to ProductStructureKernel?
        else:
            covar = covar_x

        return gpytorch.distributions.MultivariateNormal(mean_x, covar)

    def get_fantasy_model(self, inputs, targets, **kwargs):
        try:
            model_out = \
                super(MultitaskLinPlusStationaryDKLGP,self).get_fantasy_model(
                    inputs, targets, **kwargs
                )
        except RuntimeError:
            warnings.warn('Guarding failed get_fantasy_model call',
                          RuntimeWarning)
            model_out = deepcopy(self)
            # TODO: Don't I want a model_out.set_train_data here?
        return model_out

# Import model classes and tools for loading model parameters


# The key functions we'd like to emulate or modify
def compute_menu_scores_target_performance_and_cmi_monolithic(
        menu_df, target_df, model, predictor_cols, type_col, noisevar,
        target_ab_ids=None, lincoeffs_target_means_by_ab=None, cost_dict=None):
    rows = []
    target_rows = []
    antigen_sequences = []
    pam30_penalty_values = []
    for idx in range(menu_df.shape[0]):
        row_i = menu_df.iloc[[idx]]
        # Use brackets to ensure the returned slice is a DF
        # Get the antigen sequence from row_i
        # Assumes that the menu_df is NOT indexed by this value, and instead,
        # it's in the row
        antigen_sequence = row_i.iloc[0]['AntigenSequence']
        antigen_sequences.append(antigen_sequence)
        pam30_penalty_values.append(row_i.iloc[0]['PAM30Penalties'])
        # Retrieve the target rows
        target_rows_i = get_target_rows(target_df, antigen_sequence,
                                        target_ab_ids)

        rows.append(row_i)
        target_rows.append(target_rows_i)

    # pam30_penalty_values = compute_pam30_penalties(antigen_sequences)
    # Sign flip: now in range -1.0 to 0.0
    pam30_penalty_values = [-1.0 * p30i for p30i in pam30_penalty_values]

    start_rows = [0]
    all_rows = []
    for ri, tri in zip(rows, target_rows):
        start_rows.append(start_rows[-1] + len(ri) + len(tri))
        all_rows.append(ri)
        all_rows.append(tri)
    end_rows = start_rows[1:]
    start_rows = start_rows[:-1]

    # print('Starts and ends for slices:')
    # print(start_rows)
    # print(end_rows)

    # prepare the inputs to the model
    pred_x = torch.tensor(
        pd.concat(all_rows, axis=0, sort=False)[
            predictor_cols].values,
        dtype=torch.float
    )
    pred_i = torch.tensor(
        pd.concat(all_rows, axis=0, sort=False)[
            type_col].values.astype(np.double),
        dtype=torch.long
    )

    print('Obtaining single joint MVN at {} ...'.format(
        str(datetime.now().strftime('%Y%m%d_%H%M%S_%f'))))
    with torch.no_grad(), gpytorch.settings.max_root_decomposition_size(100000):
        joint_mvn = model(PRED_FEATURE_SCALING * pred_x, pred_i)
    print('Done at {}!'.format(str(datetime.now().strftime('%Y%m%d_%H%M%S_%f'))))

    exploit = []
    explore = []
    pred_mean = []
    pred_std = []
    for row_i, si, ei in zip(rows, start_rows, end_rows):
        explore_i, exploit_i, pred_mean_i, pred_std_i = get_dec_rule_val(
            row_i, joint_mvn.mean[si:ei], joint_mvn.covariance_matrix[si:ei, si:ei],
            type_col, lincoeffs_target_means_by_ab,
            noisevar, cost_dict
        )
        exploit.append(exploit_i)
        explore.append(explore_i)
        pred_mean.append(pred_mean_i)
        pred_std.append(pred_std_i)
    print('Done at {}.'.format(str(datetime.now().strftime('%Y%m%d_%H%M%S_%f'))))
    # scores = [er_i + ei_i for er_i, ei_i in zip(explore, exploit)]
    scores = [er_i + ei_i for er_i, ei_i in zip(explore, exploit)]
    for i, p30i in enumerate(pam30_penalty_values):
        if not np.isnan(p30i):
            scores[i] = scores[i] + p30i
    for er_i, ei_i, p30i, si in zip(explore, exploit, pam30_penalty_values, scores):
        # print('Explore: {}, Exploit: {}, Score: {}'.format(er_i, ei_i, si ))
        print('Explore: {}, Exploit: {}, Pam30Penalty: {}, Score: {}'.format(er_i, ei_i, p30i, si))

    return scores, pred_mean, pred_std

def get_target_rows(target_df, study_antigen_sequence, target_antibody_ids):
    """
    Given an antigen, find the target-fideilty 'studies' that correspond with it
    :param target_df:  DataFrame, indexed by AntigenSequence and AntibodyID,
        that contains the target-fidelity feature vectors.
    :param study_antigen_sequence: str, giving the antigen sequence.
    :param target_antibody_ids: list of n str, where each is the AntibodyID for a
        single, target antibody.
    :return: DataFrame containing only the n rows corresponding to the targets.
    """

    rows = []
    for abi in target_antibody_ids:
        rows.append(target_df.loc[[(study_antigen_sequence, abi)]])  # putting the tuple inside [] means that we return a DataFrfame
        assert not isinstance(rows[-1], pd.Series), 'Must return 1xk DataFrames'

        # Above: could use to_frame().transpose(), but would coerce data types.
        # Doing .to_frame().transpose() would ensure that the concatenated
        # objects are 1-row, k-column DataFrames, yielding a n-row,
        # k-column DataFrame below
    return pd.concat(rows, axis=0)

def get_dec_rule_val(row_i, joint_mvn_mean, joint_mvn_covariance, type_col, lincoeffs_target_means_by_ab, noisevar, cost_dict):

    # Get the weighted exploitation score:
    exploit = joint_mvn_mean[1:].dot(torch.tensor(lincoeffs_target_means_by_ab,
                                                  dtype=torch.float))  # TODO: check syntax
    # pred_mean.append(joint_mvn.mean[0])
    # pred_std.append(joint_mvn.stddev[0])

    # Get the weighted exploration score:
    # print('Joint covariance matrix of study of type {} and target:'.format(
    #     row_i.iloc[0][type_col]))
    # print(joint_mvn.covariance_matrix)
    cmi = compute_conditional_mi(joint_mvn_covariance, noisevar)
    # print('Calculated CMI value of {}'.format(cmi))
    explore = cmi / float(cost_dict[row_i.iloc[0][type_col]])

    return explore, exploit, joint_mvn_mean[0], np.sqrt(joint_mvn_covariance[0, 0])

def compute_conditional_mi(covariance_matrix_with_study_first, noisevar=0.0):
    """
    Computes mutual information between study and targets

    Joint distribution given as a GPyTorch MultivariateNormal in which row 0 is
    the study and all other rows are targets.

    :param covariance_matrix_with_study_first: covariance matrix from a
        MultivariateNormal of size > 1. Gives the joint distribution of the
        candidate observation and its "cognate" targets.
    :param noisevar: float giving the variance of the noise.
    :return: float giving the conditional mutual information between the
        candidate study and the targets.
    """

    if isinstance(noisevar, torch.Tensor):
        noisevar = noisevar.clone().detach().float()

    prior_var = torch.max(torch.zeros((1,)), covariance_matrix_with_study_first[0,0]) + noisevar  # What if cov matrix has negatives on diagonal; shouldn't, but I've seen it happen
    try:
        posterior_var = prior_var - \
                        gpytorch.inv_quad(
                            covariance_matrix_with_study_first[1:, 1:],
                            covariance_matrix_with_study_first[0, 1:]
                        )
        inf = -0.5 * (posterior_var.log() - prior_var.log()).float()  # there is some connection between target and candidate study
    except RuntimeError:
        # This is necessary because the (Cholesky) decomp can fail if the
        # covariance matrix is ~ singular
        warnings.warn('Guarding failed calculation of menu/target mutual information!', RuntimeWarning)
        print('Covariance matrix in failed CMI calculation:')
        print(covariance_matrix_with_study_first)
        inf = torch.tensor(0.0, dtype=torch.float)
        # Assume failure is problematic, and we should not give exploratory
        # value to this observation

    # Protect against numerical problems via the following limits:
    # TODO: Fix ranging; sometimes getting NEGATIVES or NaNs; upper lim in case variances are wrong (e.g., negative)?
    inf_upper_lim = -0.5 * (torch.log(noisevar) - prior_var.log()).float()  # target determines candidate study results up to the observation noise
    inf_lower_lim = 0.0  # target and candidate study are conditionally independent
    v = np.min([np.max([inf, inf_lower_lim]), inf_upper_lim])
    if v < 0:
        print('v < 0!')
    return v


PRED_FEATURE_SCALING = 0.1


def get_dec_rule_val_loc(joint_mvn_mean, joint_mvn_covariance, lincoeffs_target_means_by_ab, noisevar, execution_cost=1.0):

    # Get the weighted exploitation score:
    exploit = joint_mvn_mean[1:].dot(torch.tensor(lincoeffs_target_means_by_ab,
                                                  dtype=torch.float))  # TODO: check syntax
    # pred_mean.append(joint_mvn.mean[0])
    # pred_std.append(joint_mvn.stddev[0])

    # Get the weighted exploration score:
    # print('Joint covariance matrix of study of type {} and target:'.format(
    #     row_i.iloc[0][type_col]))
    # print(joint_mvn.covariance_matrix)
    cmi = compute_conditional_mi(joint_mvn_covariance, noisevar)
    # print('Calculated CMI value of {}'.format(cmi))
    explore = cmi / float(execution_cost)

    return explore, exploit, joint_mvn_mean[0], np.sqrt(joint_mvn_covariance[0, 0])


def instantiate_model(state_dict, x, i, y):
    # Instantiate the model:
    layer_sizes = get_layer_sizes(state_dict)
    num_tasks = get_num_tasks(state_dict)

    if set(layer_sizes.keys()) == {'lin_layers'}:
        # We have a MultitaskVanillaGPModelDKL model
        model = MultitaskVanillaGPModelDKL(
            (x, i), y,
            layer_sizes=layer_sizes['lin_layers'],
            num_tasks=num_tasks,
            likelihood = gpytorch.likelihoods.GaussianLikelihood(
                noise_constraint=Interval(0.0001, 1.0)
            )
        )
    elif set(layer_sizes.keys()) == {'lin_layers_lin', 'lin_layers_stationary'}:
        model = MultitaskLinPlusStationaryDKLGP(
            (x, i), y,
            layer_sizes_lin=layer_sizes['lin_layers_lin'],
            layer_sizes_stationary=layer_sizes['lin_layers_stationary'],
            num_tasks=num_tasks,
            likelihood=gpytorch.likelihoods.GaussianLikelihood(
                noise_constraint=Interval(0.0001, 1.0)
            )
        )
    else:
        raise ValueError('state_dict type not recognized!')

    print('Model initialized; applying provided model state (weights)...')
    model.load_state_dict(state_dict)
    print('... done!')

    # print_parameters(model, model.likelihood)
    model.eval()
    model.likelihood.eval()

    return model


def compute_menu_scores_target_performance_and_cmi_monolithic_local(
        menu_df, target_df, model, predictor_cols, type_col, noisevar,
        target_ab_ids=None, lincoeffs_target_means_by_ab=None, cost_dict=None
):

    # Computing some values we'll need downstream
    antigen_sequences = list(menu_df['AntigenSequence'])

    pam30_penalty_values = list(menu_df['PAM30Penalties'])
    pam30_penalty_values = [-1.0 * p30i for p30i in pam30_penalty_values]
    # Above: Sign flip: now in range -1.0 to 0.0

    row_indices_int = [idx for idx in range(menu_df.shape[0])]
    target_indices_tup = [
        [(antigen_sequence, tabidi) for tabidi in target_ab_ids] for antigen_sequence in antigen_sequences
    ]

    # Prepare the inputs to the model:
    # First, the dataframe
    start_rows = [0]
    lslices = []
    for mri, trsi in zip(row_indices_int, target_indices_tup):
        start_rows.append(start_rows[-1] + 1 + len(trsi))  # 1 here is from mri being a single int
        lslices.append(menu_df.iloc[[mri]])
        lslices.append(target_df.loc[trsi, :])
    all_rows_df = pd.concat(lslices, axis=0, sort=False)
    end_rows = start_rows[1:]
    start_rows = start_rows[:-1]


    # Then, the input tensors from that dataframe
    pred_x = torch.tensor(
        all_rows_df[predictor_cols].values,
        dtype=torch.float
    )
    pred_i = torch.tensor(
        all_rows_df[type_col].values.astype(np.double),
        dtype=torch.long
    )

    # Pass the tensors to the model for predictions:
    print('Obtaining single joint MVN at {} ...'.format(
        str(datetime.now().strftime('%Y%m%d_%H%M%S_%f'))))
    with torch.no_grad(), gpytorch.settings.max_preconditioner_size(5):
        # Other possible settings: lazily_evaluate_kernels(False): compute cross-covariance terms by default (?)
        # max_preconditioner_size(5)  # For some reason, preconditioning turned off by default (0)
        joint_mvn = model(PRED_FEATURE_SCALING * pred_x, pred_i)
    print('Done at {}!'.format(str(datetime.now().strftime('%Y%m%d_%H%M%S_%f'))))

    # # To see the difference between initialization cost and prediction cost
    # print('Rerunning the single joint MVN at {} ...'.format(
    #     str(datetime.now().strftime('%Y%m%d_%H%M%S_%f'))))
    # with torch.no_grad():
    #     _ = model(PRED_FEATURE_SCALING * pred_x, pred_i)
    # print('Done at {}!'.format(str(datetime.now().strftime('%Y%m%d_%H%M%S_%f'))))

    # Using the model predictions, compute the quantities for the decision rule
    exploit = []
    explore = []
    pred_mean = []
    pred_std = []
    # execution_costs = [cost_dict[row_i.iloc[0][type_col]] for row_i in rows]
    execution_costs = [cost_dict[tci] for tci in list(menu_df[type_col])]
    for eci, si, ei in zip(execution_costs, start_rows, end_rows):
        explore_i, exploit_i, pred_mean_i, pred_std_i = get_dec_rule_val_loc(
            joint_mvn.mean[si:ei], joint_mvn.covariance_matrix[si:ei, si:ei],
            lincoeffs_target_means_by_ab,
            noisevar, eci
        )
        exploit.append(exploit_i)
        explore.append(explore_i)
        pred_mean.append(pred_mean_i)
        pred_std.append(pred_std_i)
    print('Done at {}.'.format(str(datetime.now().strftime('%Y%m%d_%H%M%S_%f'))))
    # scores = [er_i + ei_i for er_i, ei_i in zip(explore, exploit)]
    scores = [er_i + ei_i for er_i, ei_i in zip(explore, exploit)]  # TODO: nans
    for i, p30i in enumerate(pam30_penalty_values):
        if not np.isnan(p30i):
            scores[i] = scores[i] + p30i
    # for er_i, ei_i, p30i, si in zip(explore, exploit, pam30_penalty_values, scores):
    #     # print('Explore: {}, Exploit: {}, Score: {}'.format(er_i, ei_i, si ))
    #     print('Explore: {}, Exploit: {}, Pam30Penalty: {}, Score: {}'.format(er_i, ei_i, p30i, si))

    return scores, pred_mean, pred_std


if __name__ == '__main__':
    input_pickle_path = '/Users/desautels2/GitRepositories/abag_ml/' \
                        'score_computation_inputs.pkl'
    lin_model_path = '/Users/desautels2/GitRepositories/abag_ml/abag_ml/' \
                     'tmp_data/model_20190925_232912_8.pth'

    ntrain = 10000
    npredict = 1000

    with open(input_pickle_path, 'rb') as f:
        # This pickle contains the following items:
        # menu_df
        # target_df
        # predictor_columns
        # type_col
        # noisevar
        # target_ab_ids
        # lincoeffs_target_means_by_ab
        # cost_dict
        # model_state_dict
        # model_inputs_x
        # model_inputs_i
        # model_targets_y
        d = pickle.load(f)

    lin_model_state_dict = torch.load(lin_model_path)

    ntrain = np.min([ntrain, d['model_inputs_x'].shape[0]])
    npredict = np.min([npredict, d['menu_df'].shape[0]])

    print('ntrain: {}'.format(ntrain))
    print('npredict: {}'.format(npredict))

    model = instantiate_model(
        d['model_state_dict'],
        d['model_inputs_x'][:ntrain, :],
        d['model_inputs_i'][:ntrain],
        d['model_targets_y'][:ntrain]
    )

    model_lin = instantiate_model(
        lin_model_state_dict,
        d['model_inputs_x'][:ntrain, :],
        d['model_inputs_i'][:ntrain],
        d['model_targets_y'][:ntrain]
    )

    # Locally-modified version of the routine
    mcopy = copy.deepcopy(model)
    print('Starting locally-modified score computation at {}'.format(
        datetime.now().strftime('%Y%m%d_%H%M%S_%f'))
    )
    scores_loc, pred_mean_loc, pred_std_loc = \
        compute_menu_scores_target_performance_and_cmi_monolithic_local(
            d['menu_df'].iloc[:npredict, :],
            d['target_df'],
            mcopy,
            d['predictor_columns'],
            d['type_col'],
            d['noisevar'],
            target_ab_ids=d['target_ab_ids'],
            lincoeffs_target_means_by_ab=d['lincoeffs_target_means_by_ab'],
            cost_dict=d['cost_dict']
        )
    print('Done with locally-modified score computation at {}'.format(
        datetime.now().strftime('%Y%m%d_%H%M%S_%f'))
    )

    # mcopy = copy.deepcopy(model_lin)
    # print('Starting linear model in locally-modified score computation at {}'.format(
    #     datetime.now().strftime('%Y%m%d_%H%M%S_%f'))
    # )
    # scores_lin, pred_mean_lin, pred_std_lin = \
    #     compute_menu_scores_target_performance_and_cmi_monolithic_local(
    #         d['menu_df'].iloc[:npredict, :],
    #         d['target_df'],
    #         mcopy,
    #         d['predictor_columns'],
    #         d['type_col'],
    #         d['noisevar'],
    #         target_ab_ids=d['target_ab_ids'],
    #         lincoeffs_target_means_by_ab=d['lincoeffs_target_means_by_ab'],
    #         cost_dict=d['cost_dict']
    #     )
    # print('Done with linear model in locally-modified score computation at {}'.format(
    #     datetime.now().strftime('%Y%m%d_%H%M%S_%f'))
    # )

    # Original routine:
    mcopy = copy.deepcopy(model)
    print('Starting reference score computation at {}'.format(
        datetime.now().strftime('%Y%m%d_%H%M%S_%f'))
    )
    scores_ref, pred_mean_ref, pred_std_ref = \
        compute_menu_scores_target_performance_and_cmi_monolithic(
            d['menu_df'].iloc[:npredict, :],
            d['target_df'],
            mcopy,
            d['predictor_columns'],
            d['type_col'],
            d['noisevar'],
            target_ab_ids=d['target_ab_ids'],
            lincoeffs_target_means_by_ab=d['lincoeffs_target_means_by_ab'],
            cost_dict=d['cost_dict']
        )
    print('Done with reference score computation at {}'.format(
        datetime.now().strftime('%Y%m%d_%H%M%S_%f'))
    )

    # Check the individual results of the calculation
    for name, loc_ref_tup in zip(
            ['scores', 'pred_mean', 'pred_std'],
            [(scores_loc, scores_ref),
             (pred_mean_loc, pred_mean_ref),
             (pred_std_loc, pred_std_ref)
             ]
    ):
        if all([li == ri or (np.isnan(li) and np.isnan(ri)) for li, ri in zip(loc_ref_tup[0], loc_ref_tup[1])]):
            print('All {} match!'.format(name))
        else:
            print('{} mismatches!'.format(name))
            for i, tup in enumerate(zip(scores_loc, scores_ref)):
                if tup[0] != tup[1]:
                    print(i, tup)

